﻿using UnityEngine;
using System.Collections;
using System;
using Firebase.Database;
using BicDB.Utility;
using BicDB.Variable;

namespace BicDB.Storage
{
	public class FDDataStoreContainer<T> : BicDB.Container.DataStoreContainer<T> where T : class, BicDB.IRecordContainer, new()
	{
		#region OnAdded
		private Action<string, T> onAddedRowActions;
		private bool isBindOnAddedRowActions = false;
		public override Action<string, T> OnAddedRowActions { 
			get{ 
				return onAddedRowActions;
			} 

			set{
				if (onAddedRowActions == null && isBindOnAddedRowActions == false ) {
					Firebase.Database.FirebaseDatabase.DefaultInstance.GetReference(getDataPath()).ChildAdded += NotifyAdded;
					isBindOnAddedRowActions = true;
				}

				onAddedRowActions = value;

				if (onAddedRowActions == null && isBindOnAddedRowActions == true) {
					isBindOnAddedRowActions = false;
					Firebase.Database.FirebaseDatabase.DefaultInstance.GetReference(getDataPath()).ChildAdded -= NotifyAdded;
				}
			}
		}

		private void NotifyAdded(object sender, ChildChangedEventArgs _result){
			if (!ContainsKey(_result.Snapshot.Key)) {
				string _data = _result.Snapshot.GetRawJsonValue();
				int _count = 0;
				var _newRow = new T();
				BicDB.Utility.JsonConvertor.GetInstance().BuildModelContainer(_newRow, ref _data, ref _count);
				Add(_result.Snapshot.Key, _newRow);
			}
		}

		public void AddAsync(T _item, Action<string, Result> _callback){
			string _key = Firebase.Database.FirebaseDatabase.DefaultInstance.GetReference(getDataPath()).Push().Key;

			string _json = string.Empty;
			_item.BuildFormattedString(ref _json, JsonConvertor.GetInstance());

			Firebase.Database.FirebaseDatabase.DefaultInstance.GetReference(getDataPath() + "/" + _key).SetRawJsonValueAsync(_json).ContinueWith(_task=>{
				if(_task.IsFaulted || !_task.IsCompleted){
					_callback(_key, new Result((int)FDStorage.ResultCode.FailedAsync));
				}else{
					Add(_key, _item);
					_callback(_key, new Result((int)FDStorage.ResultCode.Success));
				}
			});
		}

		private string getDataPath(){
			return header [FDStorage.HEADERKEY_DATASTORE_PATH].AsVariable.AsString + "/data";
		}
		#endregion

		#region OnRemoved
		private Action<string, T> onRemovedRowActions;
		private bool isBindOnRemovedRowActions = false;
		public override Action<string, T> OnRemovedRowActions { 
			get{ 
				return onRemovedRowActions;
			} 

			set{
				if (onRemovedRowActions == null && isBindOnRemovedRowActions == false ) {
					Firebase.Database.FirebaseDatabase.DefaultInstance.GetReference(getDataPath()).ChildRemoved += NotifyRemoved;
					isBindOnRemovedRowActions = true;
				}

				onRemovedRowActions = value;

				if (onRemovedRowActions == null && isBindOnRemovedRowActions == true) {
					isBindOnRemovedRowActions = false;
					Firebase.Database.FirebaseDatabase.DefaultInstance.GetReference(getDataPath()).ChildRemoved -= NotifyRemoved;
				}
			}
		}

		private void NotifyRemoved(object sender, ChildChangedEventArgs _result){
			Remove(_result.Snapshot.Key);
		}

		public void RemoveAsync(string _key, Action<string, Result> _callback){

			Firebase.Database.FirebaseDatabase.DefaultInstance.GetReference(getDataPath() + "/" + _key).RemoveValueAsync().ContinueWith(_task=>{
				if(_task.IsFaulted || !_task.IsCompleted){
					_callback(_key, new Result((int)FDStorage.ResultCode.FailedAsync));
				}else{
					Remove(_key);
					_callback(_key, new Result((int)FDStorage.ResultCode.Success));
				}
			});

		}
		#endregion


		public FDDataStoreContainer(string _name) : base(_name){

		}



	}
}