﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Firebase.Database;
using System.Threading.Tasks;
using System;
using BicDB.Variable;
using System.Linq;
using BicDB.Container;
using BicDB.Utility;
using Firebase;

namespace BicDB.Storage
{
	public class FDStorage : IDataStoreStorage 
	{
		#region static
		static public string HEADERKEY_DATASTORE_PATH = "firebase_path";

		static private bool isInit = false;
		static public void Init(string _databaseUrl, string _p12Filename, string _serviceAccountEmail, string _p12Password = "notasecret"){
			if (isInit == true) {
				return;
			}

			#if UNITY_EDITOR
			Firebase.Unity.Editor.FirebaseEditorExtensions.SetEditorDatabaseUrl(FirebaseApp.DefaultInstance, _databaseUrl);
			Firebase.Unity.Editor.FirebaseEditorExtensions.SetEditorP12FileName(FirebaseApp.DefaultInstance, _p12Filename);
			Firebase.Unity.Editor.FirebaseEditorExtensions.SetEditorServiceAccountEmail(FirebaseApp.DefaultInstance, _serviceAccountEmail);
			Firebase.Unity.Editor.FirebaseEditorExtensions.SetEditorP12Password(FirebaseApp.DefaultInstance, _p12Password);
			#endif

			isInit = true;
		}
		#endregion

		#region Enum

		public enum ResultCode
		{
			Success = 0,
			FailedAsync = 1
		}

		#endregion

		#region Singleton
		static private FDStorage instance = null;
		static public FDStorage GetInstance(){
			if (FDStorage.isInit == false) {
				throw new Exception("Need FDStorage.Init");
			}

			if (instance == null) {
				instance = new FDStorage();
			}

			return instance;
		}
		#endregion


		#region Storage

		public void Save<T>(BicDB.IDataStoreContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{
			throw new NotImplementedException();
		}

		public void Load<T>(BicDB.IDataStoreContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{
			_table.Clear();

			if (!_table.Header.ContainsKey(HEADERKEY_DATASTORE_PATH)) {
				throw new SystemException("FirebaseDataBase DataStoreContainer Need Header FDStorage.HEADERKEY_DATASTORE_PATH");
			}

			FirebaseDatabase.DefaultInstance.GetReference( _table.Header[HEADERKEY_DATASTORE_PATH].AsVariable.AsString).GetValueAsync().ContinueWith(task=>{
				if(task.IsFaulted || !task.IsCompleted){
					_callback(new Result((int)ResultCode.FailedAsync));
				}else{
					DataSnapshot _snapshot = task.Result;

					string _data = _snapshot.GetRawJsonValue();
					int _counter = 0;

					JsonConvertor.GetInstance().BuildDataStoreContainer(_table, ref _data, ref _counter);

					_callback(new Result((int)ResultCode.Success));
				}
			});
		}

		public void Pull<T>(BicDB.IDataStoreContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{

		}

		#endregion
	}




}