﻿using UnityEngine;
using System.Collections;
using BicDB.Container;
using System;
using Firebase.Database;
using BicDB.Utility;
using BicDB.Variable;

namespace BicDB.Storage
{
	//test table
	public class FDRecordContainer : RecordContainer{

		private Action<IRecordContainer, string> onChangedValueActions;
		private bool isBindOnChagedValueActions = false;
		public override Action<IRecordContainer, string> OnChangedValueActions{ 
			get{ 
				return onChangedValueActions;
			} 

			set{
				if (onChangedValueActions == null && isBindOnChagedValueActions == false ) {
					string _path = getDataPath() + "/" + Parent.GetRecordKey(this).AsString;
					FirebaseDatabase.DefaultInstance.GetReference(_path).ValueChanged += NotifyChanged;
					isBindOnChagedValueActions = true;
				}

				onChangedValueActions = value;

				if (onChangedValueActions == null && isBindOnChagedValueActions == true) {
					isBindOnChagedValueActions = false;
					string _path = getDataPath() + "/" + Parent.GetRecordKey(this).AsString;
					FirebaseDatabase.DefaultInstance.GetReference(_path).ValueChanged -= NotifyChanged;
				}
			}
		}

		private void NotifyChanged(object sender, ValueChangedEventArgs _result){
			if (_result.DatabaseError != null) {
				Debug.Log(_result.DatabaseError.Message);
			}
			string _data = _result.Snapshot.GetRawJsonValue();
			int _count = 0;

			if (!string.IsNullOrEmpty(_data)) {
				JsonConvertor.GetInstance().BuildModelContainer(this, ref _data, ref _count);
				NotifyChanged();
			}

		}

		public void Async(Action<string,Result> _callback){
			string _key = Parent.GetRecordKey(this).AsString;
			string _path = getDataPath() + "/" + _key;
			string _json = string.Empty;

			this.BuildFormattedString(ref _json, JsonConvertor.GetInstance());

			FirebaseDatabase.DefaultInstance.GetReference(_path).SetRawJsonValueAsync(_json).ContinueWith(_task=>{
				if(_task.IsFaulted || !_task.IsCompleted){
					_callback(_key, new Result((int)FDStorage.ResultCode.FailedAsync));
				}else{
					_callback(_key, new Result((int)FDStorage.ResultCode.Success));
				}
			});
		}

		private string getDataPath(){
			return Parent.Header [FDStorage.HEADERKEY_DATASTORE_PATH].AsVariable.AsString + "/data";
		}

	}
}